package com.xsis.javabootcamp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.javabootcamp.model.Category;
import com.xsis.javabootcamp.model.Variant;
import com.xsis.javabootcamp.service.CategoryService;
import com.xsis.javabootcamp.service.VariantService;

@Controller
@RequestMapping("/variant")
public class VariantController {

	private VariantService variantService;
	
	private CategoryService categoryService;
	
	// jika service yg di inject lebih dari 1, gunakan constructor untuk autowire nya
	public VariantController(VariantService variantService, CategoryService categoryService) {
		super();
		this.variantService = variantService;
		this.categoryService = categoryService;
	}

	@GetMapping("/")
	public String categories(Model model) {
		model.addAttribute("variants", variantService.findVariants());
		
		return "variant/variant.html";
	}
	
	@GetMapping("new")
	public String newvariant(Model model) {
		model.addAttribute("variant", new Variant());
		model.addAttribute("categories", categoryService.findCategories());
		return "variant/add";
	}
	
	@GetMapping("edit/{id}")
	public String editvariant(@PathVariable(name = "id") Long id, Model model) {
		
		model.addAttribute("variant", variantService.findVariantById(id));
		model.addAttribute("categories", categoryService.findCategories());
		return "variant/edit.html";
	}
	
	@PostMapping("save")
	public String saveCate(@Valid Variant variant,Errors errors) {
		if (errors.hasErrors()) {
			return "variant/add";
		}
		variantService.save(variant);
		return "redirect:/variant/";
	}	
	
	@GetMapping("delete/{id}")
	public String deleteVariant(@PathVariable(name = "id") Long id) {
		variantService.deleteVariantById(id);
		
		return "redirect:/variant/";
	}
	
}
