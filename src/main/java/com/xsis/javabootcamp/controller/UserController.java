package com.xsis.javabootcamp.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.javabootcamp.model.User;
import com.xsis.javabootcamp.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("/")
	public String findAllUser(Model model) {
		model.addAttribute("userList", userService.findAllUser());
		return "/user/users";
	}
	
	@GetMapping("new")
	public String addUser(Model model) {
		 model.addAttribute("user", new User());
		 return "user/add";
	}

	@PostMapping("save")
	public String saveUser(@Valid User user, Errors errors) {
		userService.saveUser(user);
		return "redirect:/user/";
	}
	
	@GetMapping("edit/{id}")
	public String findUserById(@PathVariable(name = "id") Long id, Model model) {
		
		Optional<User> user = userService.findByUserId(id);
		model.addAttribute("user", user);
		return "user/edit";
		
	}
	
	@GetMapping("delete/{id}")
	public String deleteUser(@PathVariable(name = "id") Long id) {
		userService.deleteUserById(id);
		
		return "redirect:/user/";
	}
}
