package com.xsis.javabootcamp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xsis.javabootcamp.model.Category;
import com.xsis.javabootcamp.service.CategoryService;

@Controller
@RequestMapping(value="/category")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/")
	public String categories(Model model) {
		model.addAttribute("categories", categoryService.findCategories());
		return "category/category.html";
	}
	
	@GetMapping("new")
	public String newCate(Model model) {
		model.addAttribute("cate", new Category());
		return "category/add";
	}
	
	@GetMapping("edit/{id}")
	public String editCate(@PathVariable(name = "id") Long id, Model model) {
		
		model.addAttribute("cate", categoryService.findCategoryById(id));
		return "category/edit";
	}
	
	@PostMapping("save")
	public String saveCate(@Valid Category cate,Errors errors) {
		categoryService.save(cate);
		return "redirect:/category/";
	}	
	
	@GetMapping("delete/{id}")
	public String deleteCate(@PathVariable(name = "id") Long id) {
		categoryService.deleteCategoryById(id);
		
		return "redirect:/category/";
	}
	

}
