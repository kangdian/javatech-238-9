package com.xsis.javabootcamp.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.xsis.javabootcamp.model.User;

@Controller
public class IndexController {
	
	@GetMapping("/")
	public String index() {
		return "index";
	}
	
	@GetMapping("/usersx")
	public String showUser(Model model) {
		List<User> users= Arrays.asList(
				new User(new Long(121),"xsis","xsis@gmail.com","x123"),
				new User(new Long(122),"xa","xa@gmail.com","xa"),
				new User(new Long(123),"academy","academy@gmail.com","xacademy")
				);
		
		model.addAttribute("userList",users);
		return "user/users";
	}
	
	@GetMapping("/rest/category")
	public String showRestCategory() {
		return "category-rest/category.html";
	}
	
	@GetMapping("/rest/category/add")
	public String showRestCategoryAdd() {
		return "category-rest/addEdit.html";
	}
	
	@GetMapping("/rest/variant")
	public String showRestVariant() {
		return "variant/variant-rest.html";
	}
	
	@GetMapping("/rest/variant/newModal")
	public String addVariantModal() {
		return "variant/addEdit.html";
	}
	
	@GetMapping("/rest/product")
	public String showRestProduct() {
		return "product/product-rest.html";
	}
	
	@GetMapping("/rest/product/newModal")
	public String addProductModal() {
		return "product/addEdit.html";
	}
}
