package com.xsis.javabootcamp.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.javabootcamp.model.Product;
import com.xsis.javabootcamp.model.dto.ProductDto;
import com.xsis.javabootcamp.service.ProductService;

@RestController
@RequestMapping(path = "/api/product", produces = "application/json")
@CrossOrigin(origins = "*")
public class ProductRestController {
	@Autowired
	private ProductService productService;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllProduct() {
		return new ResponseEntity<>(productService.findAllProduct(), HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<?> saveProduct(@RequestBody Product product) {
		return new ResponseEntity<>(productService.save(product), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?>  deleteProduct(@PathVariable("id") Long productId) {
		try {
			productService.delete(productId);
		} catch (EmptyResultDataAccessException e) {
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
					.body("{delete failed}");
		}
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/put")
	public ResponseEntity<?> putProduct(@RequestBody Product product) {
		return new ResponseEntity<>(productService.save(product), HttpStatus.OK);
	}
	
	/*--- all product crud use dto implementation ---*/
	@GetMapping("/fetchDto")
	public ResponseEntity<?> queryProduct(@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType) {
		
		return new ResponseEntity<>(productService.queryProduct(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@GetMapping("/findProductByDto/{id}")
	public ResponseEntity<?> findProductDtoById(@PathVariable("id") Long id) {
		
		return new ResponseEntity<>(productService.findProductDtoById(id), HttpStatus.OK);
	}
	
	@PostMapping("/saveDto")
	public ResponseEntity<?> saveProductDto(@RequestBody ProductDto productDto) {
		return new ResponseEntity<>(productService.saveProductDto(productDto), HttpStatus.OK);
	}
	
	@GetMapping("/searchBy")
	public ResponseEntity<?> searchProductBy(@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType,
			@RequestParam String initial,@RequestParam String name,@RequestParam String description) {
		
		return new ResponseEntity<>(productService.searchProductBy(pageNo, pageSize, sortBy, sortType, initial, name, description), HttpStatus.OK);
	}
	
	@GetMapping("updateIsDelete/{id}")
	public ResponseEntity<?>  setIsDelete(@PathVariable(name = "id") Long id) {
		int rowDelete =0;
		try {
			 rowDelete=productService.setProductIsDelete(id);
		} catch (EmptyResultDataAccessException e) {
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
					.body("{delete failed}");
		}
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
		        .body("[{ \"message\": \"delete succeed "+rowDelete+" row\"}]");
		

	}

	
}
