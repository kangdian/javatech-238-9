package com.xsis.javabootcamp.service;

import java.util.List;
import java.util.Optional;

import com.xsis.javabootcamp.model.User;

public interface UserService {
	List<User> findAllUser();
	
	// method to save User
	void saveUser(User user);
	
	Optional<User> findByUserId(Long userId);
	
	void deleteUserById(Long id);
	

}
