package com.xsis.javabootcamp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.javabootcamp.model.Product;
import com.xsis.javabootcamp.model.Variant;
import com.xsis.javabootcamp.model.dto.ProductDto;
import com.xsis.javabootcamp.repository.ProductRepository;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepo;
	
	@Override
	public Product save(Product product) {
		// TODO Auto-generated method stub
		return productRepo.save(product);
	}

	@Override
	public void delete(Long productId) {
		productRepo.deleteById(productId);
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<Product> findAllProduct() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}

	@Override
	public List<ProductDto> queryProduct(Integer pageNo, Integer pageSize, String sortBy, String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<ProductDto> pageResult = productRepo.fetchProductVariant(pageable);
		return pageResult.getContent();
	}

	@Override
	public List<ProductDto> searchProductBy(Integer pageNo, Integer pageSize, String sortBy, String sortType,
			String initial, String name, String description) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<ProductDto> pageResult = productRepo.searchProductBy(pageable, initial, name, description);
		return pageResult.getContent();
	}

	@Override
	public Product saveProductDto(ProductDto productDto) {
		// TODO Auto-generated method stub
		ProductDto prod = productDto;
		Variant varn = new Variant(prod.getVarnId());
		Product product = new Product(prod.getProductId(),prod.getInitial(),
				prod.getName(),prod.getDescription(),prod.getPrice(),
				prod.getStock(),prod.isActive(),varn);
		
		
		return productRepo.save(product);
	}

	@Override
	public ProductDto findProductDtoById(Long id) {
		// TODO Auto-generated method stub
		return productRepo.findProductDtoById(id);
	}

	@Override
	public int setProductIsDelete(Long id) {
		// TODO Auto-generated method stub
		return productRepo.setProductIsDelete(id);
	}
	
	
	
	
	


}
