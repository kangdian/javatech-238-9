package com.xsis.javabootcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.javabootcamp.model.Category;
import com.xsis.javabootcamp.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	CategoryRepository cateRepo;

	@Override
	public List<Category> findCategories() {
		// TODO Auto-generated method stub
		return cateRepo.findAll();
	}

	@Override
	public Category save(Category category) {
		// TODO Auto-generated method stub
		return cateRepo.save(category);
	}

	@Override
	public Optional<Category> findCategoryById(Long id) {
		// TODO Auto-generated method stub
		return cateRepo.findById(id);
	}

	@Override
	public void deleteCategoryById(Long id) {
		// TODO Auto-generated method stub
		cateRepo.deleteById(id);
	}

}
