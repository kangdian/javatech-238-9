package com.xsis.javabootcamp.service;

import java.util.List;
import java.util.Optional;

import com.xsis.javabootcamp.model.Category;

public interface CategoryService {
	List<Category> findCategories();
	
	Category save(Category category);
	
	Optional<Category> findCategoryById(Long id);
	
	void deleteCategoryById(Long id);
}
