package com.xsis.javabootcamp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.javabootcamp.model.Variant;
import com.xsis.javabootcamp.model.dto.VariantDto;
import com.xsis.javabootcamp.repository.VariantRepository;

@Service
@Transactional
public class VariantServiceImpl implements VariantService{



	@Autowired
	private VariantRepository variantRepo;
	
	@Override
	public List<Variant> findVariants() {
		// TODO Auto-generated method stub
		return variantRepo.findAll();
	}

	@Override
	public Variant save(Variant variant) {
		// TODO Auto-generated method stub
		return variantRepo.save(variant);
	}

	@Override
	public Optional<Variant> findVariantById(Long id) {
		// TODO Auto-generated method stub
		return variantRepo.findById(id);
	}

	@Override
	public void deleteVariantById(Long id) {
		// TODO Auto-generated method stub
		variantRepo.deleteById(id);
	}
	
	@Override
	public List<VariantDto> queryVariant() {
		// TODO Auto-generated method stub
		return variantRepo.queryVariant();
	}

	@Override
	public List<VariantDto> queryVariantByCateId(Long id) {
		// TODO Auto-generated method stub
		return variantRepo.queryVariantByCategoryId(id);
	}

	@Override
	public int setIsDelete(Long id) {
		// TODO Auto-generated method stub
		return variantRepo.setIsDelete(id);
	}
	

	@Override
	public List<VariantDto> queryVariantByName(String name, String initial) {
		// TODO Auto-generated method stub
		return variantRepo.queryVariantByName(name.toUpperCase(), initial.toUpperCase());
	}
	
	


}
