package com.xsis.javabootcamp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.javabootcamp.model.User;
import com.xsis.javabootcamp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {


	@Autowired
	private UserRepository userRepo;
	
	@Override
	public List<User> findAllUser() {
		
		return userRepo.findAll();
	}

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		userRepo.save(user);
		
	}

	@Override
	public Optional<User> findByUserId(Long userId) {
		// TODO Auto-generated method stub
		return userRepo.findById(userId);
	}
	
	
	@Override
	public void deleteUserById(Long id) {
		// TODO Auto-generated method stub
		userRepo.deleteById(id);
		
	}
	
	
	

}
