package com.xsis.javabootcamp.service;

import java.util.List;
import java.util.Optional;


import com.xsis.javabootcamp.model.Variant;
import com.xsis.javabootcamp.model.dto.VariantDto;

public interface VariantService {
	List<Variant> findVariants();
	
	Variant save(Variant variant);
	
	Optional<Variant> findVariantById(Long id);
	
	void deleteVariantById(Long id);
	
	// DTO ---
	
	List<VariantDto> queryVariant();
	
	List<VariantDto> queryVariantByCateId(Long id);
	
	int setIsDelete(Long id);
	
	List<VariantDto> queryVariantByName(String name, String initial);
}
