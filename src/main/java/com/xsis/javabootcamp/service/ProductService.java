package com.xsis.javabootcamp.service;

import java.util.List;

import com.xsis.javabootcamp.model.Product;
import com.xsis.javabootcamp.model.dto.ProductDto;

public interface ProductService {
	List<Product> findAllProduct();
	
	Product save(Product product);
	
	void delete(Long productId);
	
	// JPQL
	List<ProductDto> queryProduct(Integer pageNo, Integer pageSize, String sortBy,String sortType);
	
	List<ProductDto> searchProductBy(Integer pageNo, Integer pageSize, String sortBy,String sortType,String initial,String name, String description);
	
	//save to dto
	Product saveProductDto(ProductDto productDto);
	
	ProductDto findProductDtoById(Long id);
	
	int setProductIsDelete(Long id);
}
