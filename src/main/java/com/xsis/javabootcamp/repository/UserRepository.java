package com.xsis.javabootcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.javabootcamp.model.User;

public interface UserRepository extends JpaRepository<User,Long>{

}
