package com.xsis.javabootcamp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsis.javabootcamp.model.Variant;
import com.xsis.javabootcamp.model.dto.VariantDto;

public interface VariantRepository extends JpaRepository<Variant,Long>{
	
	@Query(value="select new com.xsis.javabootcamp.model.dto.VariantDto(v.varnId,"
			+ "v.initial,v.name,v.active,c.id,c.name) "
			+ " from Variant v join Category c on v.category.id=c.id where v.isDelete=false")
	List<VariantDto> queryVariant();
	
	@Query(value="select new com.xsis.javabootcamp.model.dto.VariantDto(v.varnId,"
			+ "v.initial,v.name,v.active,c.id,c.name) "
			+ " from Variant v join Category c on v.category.id=c.id and v.varnId=?1")
	List<VariantDto> queryVariantByCategoryId(Long categoryId);
	
	@Query(value="select new com.xsis.javabootcamp.model.dto.VariantDto(v.varnId,"
			+ "v.initial,v.name,v.active,c.id,c.name) "
			+ " from Variant v join Category c on v.category.id=c.id "
			+ " where  (v.name like %:name% or v.initial like %:initial%) and v.isDelete=false")
	List<VariantDto> queryVariantByName(String name, String initial);
	
	
	@Modifying
	@Query("update Variant v set v.isDelete=true where v.varnId=?1")
	int setIsDelete(Long id);

}
