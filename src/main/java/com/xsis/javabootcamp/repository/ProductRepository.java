package com.xsis.javabootcamp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsis.javabootcamp.model.Product;
import com.xsis.javabootcamp.model.dto.ProductDto;

public interface ProductRepository extends JpaRepository<Product,Long> {
	
	@Query("select new com.xsis.javabootcamp.model.dto.ProductDto("
			+ "p.productId,p.initial,p.name,p.description, " + 
			" p.price,p.stock,p.active,p.variant.varnId,v.name,c.id,c.name) " + 
			" from Product p " + 
			" join Variant v on v.varnId=p.variant.varnId " + 
			" join Category c on c.id=v.category.id "+
			" where p.isDelete=false")
	Page<ProductDto> fetchProductVariant(Pageable pageable);
	
	@Query("select new com.xsis.javabootcamp.model.dto.ProductDto("
			+ "p.productId,p.initial,p.name,p.description, " + 
			" p.price,p.stock,p.active,p.variant.varnId,v.name,c.id,c.name) " + 
			" from Product p " + 
			" join Variant v on v.varnId=p.variant.varnId " + 
			" join Category c on c.id=v.category.id "+
			" where (p.initial like %:initial% or p.name like %:name% or p.description like %:description%)")
	Page<ProductDto> searchProductBy(Pageable pageable,String initial,String name, String description);
	
	@Query("select new com.xsis.javabootcamp.model.dto.ProductDto("
			+ "p.productId,p.initial,p.name,p.description, " + 
			" p.price,p.stock,p.active,p.variant.varnId,v.name,c.id,c.name) " + 
			" from Product p " + 
			" join Variant v on v.varnId=p.variant.varnId " + 
			" join Category c on c.id=v.category.id "+
			" where p.productId=?1")
	ProductDto findProductDtoById(Long id);
	
	@Modifying
	@Query("update Product p set p.isDelete=true where p.productId=?1")
	int setProductIsDelete(Long id);
}
