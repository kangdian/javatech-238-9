package com.xsis.javabootcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.javabootcamp.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Long>{

}
