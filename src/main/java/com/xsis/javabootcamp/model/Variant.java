package com.xsis.javabootcamp.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.xsis.javabootcamp.abstraction.SuperUser;

@Entity
@Table(name = "variant")
public class Variant extends SuperUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "varn_id")
	private Long varnId;

	@Column(name = "varn_initial")
	@Size(min = 2, max = 5)
	@NotBlank(message = "initial harus diisi")
	private String initial;

	@Column(name = "varn_name")
	@NotBlank(message = "variant name harus diisi")
	@Size(min = 3, max = 25)
	private String name;

	@Column(name = "varn_active")
	private boolean active;


	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name="varn_cate_id",nullable=true)
	private Category category;

	
	/*
	 * @OneToMany(mappedBy="variant", cascade =CascadeType.ALL,fetch=FetchType.LAZY)
	 * 
	 * @JsonManagedReference private Set<Product> products;
	 */
	 


	public Variant() {
		super();
	}

	public Variant(Long varnId, @Size(min = 2, max = 5) @NotBlank(message = "initial harus diisi") String initial,
			@NotBlank(message = "variant name harus diisi") @Size(min = 3, max = 25) String name, boolean active,
			Category category) {
		super();
		this.varnId = varnId;
		this.initial = initial;
		this.name = name;
		this.active = active;
		this.category = category;
	}
	
	

	public Variant(Long varnId) {
		super();
		this.varnId = varnId;
	}

	public Long getVarnId() {
		return varnId;
	}

	public void setVarnId(Long varnId) {
		this.varnId = varnId;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
