package com.xsis.javabootcamp.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.xsis.javabootcamp.abstraction.SuperUser;

@Entity
@Table(name = "category")
public class Category extends SuperUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "initial")
	@Size(min = 3, max = 5)
	private String initial;

	@Column(name = "name")
	@Size(min = 3, max = 25)
	private String name;

	@Column(name = "active")
	private boolean active;

	@OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private Set<Variant> variants;
	
	

	public Category() {
		super();
	}

	public Category(Long id, @Size(min = 3, max = 5) String initial, @Size(min = 3, max = 25) String name,
			boolean active) {
		super();
		this.id = id;
		this.initial = initial;
		this.name = name;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
