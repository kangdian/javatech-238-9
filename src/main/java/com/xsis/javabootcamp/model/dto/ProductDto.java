package com.xsis.javabootcamp.model.dto;

import com.xsis.javabootcamp.model.Product;

public class ProductDto {
	
	private Long productId;
	private String initial;
	private String name;
	private String description;
	private double price;
	private double stock;
	private boolean active;
	private Long varnId;
	private String varnName;
	private Long categoryId;
	private String categoryName;
	
	public ProductDto(Long productId, String initial, String name, String description, double price, double stock,
			boolean active, Long varnId, String varnName, Long categoryId, String categoryName) {
		super();
		this.productId = productId;
		this.initial = initial;
		this.name = name;
		this.description = description;
		this.price = price;
		this.stock = stock;
		this.active = active;
		this.varnId = varnId;
		this.varnName = varnName;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getStock() {
		return stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getVarnId() {
		return varnId;
	}

	public void setVarnId(Long varnId) {
		this.varnId = varnId;
	}

	public String getVarnName() {
		return varnName;
	}

	public void setVarnName(String varnName) {
		this.varnName = varnName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
}
