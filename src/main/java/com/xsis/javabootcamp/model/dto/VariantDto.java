package com.xsis.javabootcamp.model.dto;



public class VariantDto {
	private Long varnId;
	private String initial;
	private String name;
	private boolean active;
	private Long categoryId;
	private String categoryName;
	
	public VariantDto(Long varnId, String initial, String name, boolean active, Long categoryId, String categoryName) {
		super();
		this.varnId = varnId;
		this.initial = initial;
		this.name = name;
		this.active = active;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}

	public Long getVarnId() {
		return varnId;
	}

	public void setVarnId(Long varnId) {
		this.varnId = varnId;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
	
	
	
}
