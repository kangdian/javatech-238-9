package com.xsis.javabootcamp.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.xsis.javabootcamp.abstraction.SuperUser;

@Entity
@Table(name="Product")
public class Product extends SuperUser{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pro_id")
	private Long productId;
	
	@Column(name="initial")
	@Size(min=3,max=5)
	private String initial;
	
	@Column(name="name")
	@Size(min=3,max=25)
	private String name;
	
	
	@Column(name="description")
	@Size(min=3,max=255)
	private String description;
	
	
	@Column(name="price")
	@Digits(integer=8,fraction=2)
	private double price;
	
	@Column(name="stock")
	private double stock;
	
	
	@Column(name="active")
	private boolean active;
	
<<<<<<< HEAD
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "prod_varn_id", nullable = true)
=======
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "prod_varn_id")
>>>>>>> day-07
	private Variant variant;

	
	
	public Product() {
		super();
	}



	public Product(Long productId, @Size(min = 3, max = 5) String initial, @Size(min = 3, max = 25) String name,
			@Size(min = 3, max = 255) String description, @Digits(integer = 8, fraction = 2) double price, double stock,
			boolean active, Variant variant) {
		super();
		this.productId = productId;
		this.initial = initial;
		this.name = name;
		this.description = description;
		this.price = price;
		this.stock = stock;
		this.active = active;
		this.variant = variant;
	}




	public Long getProductId() {
		return productId;
	}



	public void setProductId(Long productId) {
		this.productId = productId;
	}



	public String getInitial() {
		return initial;
	}



	public void setInitial(String initial) {
		this.initial = initial;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public double getStock() {
		return stock;
	}



	public void setStock(double stock) {
		this.stock = stock;
	}



	public boolean isActive() {
		return active;
	}



	public void setActive(boolean active) {
		this.active = active;
	}



	public Variant getVariant() {
		return variant;
	}



	public void setVariant(Variant variant) {
		this.variant = variant;
	}
	
	
	
	

}
